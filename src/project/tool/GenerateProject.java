package project.tool;

import android.graphics.Color;
import android.widget.ImageView;
import android.widget.TextView;

public class GenerateProject {

	private final static String CMD_PROJECTS = "projects";
	private final static String CMD_SERVICE = "service";

	public static void main(String[] args) {
		if (args.length < 1) {
			System.out
					.println(">>>>>>缺少参数，必须输入要打包的模块名（如：java -jar tool.jar service）\r\n>>>>>>可以通过\"java -jar tool.jar projects\"命令查看模块列表");
			System.exit(1);
		}
		String cmd = args[0];
		if (CMD_PROJECTS.equalsIgnoreCase(cmd)) {
			System.out.println("project list:");
			System.out.println("\t\t" + CMD_SERVICE);
		} else if (CMD_SERVICE.equalsIgnoreCase(cmd)) {
			generateService();
		}
	}

	private static void generateService() {
		System.out.println("====generateService====");
	}

}
