package asysbang.android.developer.activity;

import android.content.res.Configuration;
import android.os.Bundle;
import asysbang.android.developer.R;

public class WeekViewActivity extends BaseActivity {
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_week_view);
		System.out.println("=========onCreate===");
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		System.out.println("=========onDestroy===");
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
		System.out.println("=========onConfigurationChanged===");
	}


}
