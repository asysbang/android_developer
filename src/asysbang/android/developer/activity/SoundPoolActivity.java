package asysbang.android.developer.activity;

import android.app.Activity;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.View;
import asysbang.android.developer.R;

public class SoundPoolActivity extends Activity {

	private SoundPool mSp;

	private int beepId, warningId, curPlayingId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_soundpool);
		mSp = new SoundPool(2, AudioManager.STREAM_MUSIC, 1);
		beepId = mSp.load(this, R.raw.beep, 1);
		warningId = mSp.load(this, R.raw.warning, 1);
		System.out.println("=====beepId====="+beepId);
		System.out.println("=====warningId====="+warningId);
	}

	public void play1(View v) {
		stop(null);
		System.out.println("=====play1====beepId="+beepId);
		AudioManager am = (AudioManager) getSystemService(AUDIO_SERVICE);// 实例化
		float audioMaxVolum = am.getStreamMaxVolume(AudioManager.STREAM_MUSIC);// 音效最大值
		float audioCurrentVolum = am.getStreamVolume(AudioManager.STREAM_MUSIC);
		float audioRatio = audioCurrentVolum / audioMaxVolum;
		curPlayingId = mSp.play(beepId, audioRatio, audioRatio, 1, -1, 1);
	}

	public void stop(View v) {
		if (curPlayingId > 0) {
			mSp.pause(curPlayingId);
			curPlayingId = -1;
		}
	}

	public void play2(View v) {
		stop(null);
		System.out.println("=====play2===warningId=="+warningId);
		AudioManager am = (AudioManager) getSystemService(AUDIO_SERVICE);// 实例化
		float audioMaxVolum = am.getStreamMaxVolume(AudioManager.STREAM_MUSIC);// 音效最大值
		float audioCurrentVolum = am.getStreamVolume(AudioManager.STREAM_MUSIC);
		float audioRatio = audioCurrentVolum / audioMaxVolum;
		curPlayingId = mSp.play(warningId, audioRatio, audioRatio, 1, -1, 1);
	}

}
