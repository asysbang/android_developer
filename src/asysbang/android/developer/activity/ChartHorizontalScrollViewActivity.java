package asysbang.android.developer.activity;

import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout.LayoutParams;
import android.widget.LinearLayout;
import asysbang.android.developer.R;
import asysbang.android.developer.ui.ChartHorizontalScrollView;
import asysbang.android.developer.ui.ChartHorizontalScrollViewItem;

public class ChartHorizontalScrollViewActivity extends Activity {
	
	ChartHorizontalScrollView mScrollView;
	
	LinearLayout mContainer;
	
	private long startTime, endTime;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_chart_horizontal_scroll_view);
		makeupData();
		
		mScrollView =(ChartHorizontalScrollView) findViewById(R.id.scrollview);
		mContainer = (LinearLayout) findViewById(R.id.scrollview_container);
		LayoutParams p = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		
		for (long time = startTime;time < endTime;) {
			Calendar c = Calendar.getInstance();
			c.setTimeInMillis(time);
//			System.out.println("========="+c.get(Calendar.MONTH)+"-"+c.get(Calendar.DAY_OF_MONTH));
			int month = c.get(Calendar.MONTH) +1;
			time += 24 * 3600 * 1000;
			ChartHorizontalScrollViewItem item = new ChartHorizontalScrollViewItem(this,month+"-"+c.get(Calendar.DAY_OF_MONTH));
			mContainer.addView(item,p);
		}
	}
	
	private void makeupData() {
		//月从0 开始计算
		Date d = new Date(2015, 3, 23);
		System.out.println("======="+d.toString());
		startTime = d.getTime();
		Date d1 = new Date(2015, 5, 11);
		endTime  = d1.getTime();
	}

}
