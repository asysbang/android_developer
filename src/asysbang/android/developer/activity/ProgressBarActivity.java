package asysbang.android.developer.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ProgressBar;
import asysbang.android.developer.R;

public class ProgressBarActivity extends BaseActivity {
	
	ProgressBar mBar ;
	private int curValue ;
	
	private Handler mHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			if (curValue <= 100) {
				mBar.setProgress(++curValue);
				sendEmptyMessageDelayed(0, 100);
			}
			if (curValue == 60) {
				mBar.setProgressDrawable(getResources().getDrawable(R.drawable.progress_horizontal_ok));
			}
		}
		
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_progress_bar);
		mBar = (ProgressBar) findViewById(R.id.progress_bar);
		mBar.setMax(100);

	}
	
	public void start(View v) {
		curValue = 0;
		mBar.setProgressDrawable(getResources().getDrawable(R.drawable.progress_horizontal));
		mHandler.sendEmptyMessage(0);
	}
	
}
