package asysbang.android.developer.ui.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.text.Layout.Alignment;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.view.View;

public class SelectedTextView extends View {

	private String text = "Once  upon a time, some children were playing at seaside when they found a turtle.They began to beat the turtle. Just at that time, "
			+ "a young man came and said to them, \"Stop!\" The children ran #(1,10) quickly. The turtle was very thankful and said, kindness. I really would like #(2,8) you"
			+ " to a wonderful palace now.";
	private TextPaint paint = new TextPaint();
	private StaticLayout sl;

	public SelectedTextView(Context context) {
		super(context);
		setBackgroundColor(Color.WHITE);
		paint.setColor(Color.BLACK);
		paint.setTextSize(38);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		sl = new StaticLayout(text, paint, canvas.getWidth(), Alignment.ALIGN_NORMAL, 1.0f, 0.0f, true);
		sl.draw(canvas);
		int offset = text.indexOf("#");
		System.out.println("==offset==="+offset);
		int line = sl.getLineForOffset(offset);
		System.out.println("=--====line===="+line);
		int offsetToLeftOf = sl.getOffsetToLeftOf(offset);
		System.out.println("========offsetToLeftOf======="+offsetToLeftOf);
		float desiredWidth = StaticLayout.getDesiredWidth(text, 0, offset, paint);
		System.out.println("======desiredWidth===="+desiredWidth);
		int start = sl.getLineStart(line);
		char charAt = text.charAt(start);
		System.out.println("=======charAt==="+charAt);
		System.out.println("=====start========"+start);
		int lineTop = sl.getLineTop(line);
		int lineBottom = sl.getLineBottom(line);
		float width = StaticLayout.getDesiredWidth(" #(1,10)", paint);
		float left = StaticLayout.getDesiredWidth(text, start, offset, paint);
		System.out.println("========lineTop====="+lineTop);
		Paint paint1 = new Paint();
		paint1.setColor(Color.BLUE);
		canvas.drawLine(left, lineTop, left+width, lineTop, paint1);
		canvas.drawLine(left, lineBottom,  left+width, lineBottom, paint1);
		canvas.drawLine(left, lineTop, left, lineBottom, paint1);
		canvas.drawLine( left+width, lineTop, left+ width, lineBottom, paint1);
	}

}
