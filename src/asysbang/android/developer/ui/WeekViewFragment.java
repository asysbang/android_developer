package asysbang.android.developer.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ViewSwitcher;
import android.widget.ViewSwitcher.ViewFactory;
import asysbang.android.developer.R;

public class WeekViewFragment extends Fragment implements ViewFactory {
	
	private ViewSwitcher mSwitcher;
	private static final int VIEW_ID = 1;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.week_view, null);
        mSwitcher = (ViewSwitcher) v.findViewById(R.id.switcher);
        mSwitcher.setFactory(this);
        mSwitcher.getCurrentView().requestFocus();
		return v;
	}


	@Override
	public View makeView() {
		WeekView view = new WeekView(getActivity(),mSwitcher);
		view.setId(VIEW_ID);
        view.setLayoutParams(new ViewSwitcher.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		return view;
	}
}
