package asysbang.android.developer.ui;

import java.lang.reflect.Field;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.HorizontalScrollView;
import android.widget.OverScroller;

public class ChartHorizontalScrollView extends HorizontalScrollView {

	OverScroller scroller;
	
	public ChartHorizontalScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
		String className = "android.widget.HorizontalScrollView";
		try {
			Class parentClass = Class.forName(className);
			Field field = parentClass.getDeclaredField("mScroller");
			field.setAccessible(true);
			scroller = (OverScroller) field.get(this);
		} catch  (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		if (ev.getAction() == MotionEvent.ACTION_UP) {
			String className = "android.widget.HorizontalScrollView";
			try {
				Class parentClass = Class.forName(className);
				Field field = parentClass.getDeclaredField("mIsBeingDragged");
				field.setAccessible(true);
				boolean mIsBeingDragged =  (Boolean) field.get(this);
					System.out.println("====onTouchEvent=======mIsBeingDragged==" +mIsBeingDragged);
			} catch (Exception e) {
				System.out.println("====onTouchEvent====Exception======");
				e.printStackTrace();
			}
		}
		return super.onTouchEvent(ev);
	}
	
	private int childWidth = 0;
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		// TODO Auto-generated method stub
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		
		int count = getChildCount();
		if (count > 0) {
			childWidth = getChildAt(0).getMeasuredWidth() /100;
			System.out.println("=======childWidth===="+childWidth);
		}
	}
	
	@Override
	public void computeScroll() {
			if (scroller.computeScrollOffset()) {
				if (scroller.isFinished()) {
					int currX = scroller.getCurrX();
					int deta = currX%childWidth;
					if (deta < childWidth/2) {
						scrollBy(deta, 0);
					} else {
						scrollBy(-deta, 0);
					}
					System.out.println("===="+deta);
				}
			}
		super.computeScroll();
	}
	
}
