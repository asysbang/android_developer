package asysbang.android.developer.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ViewSwitcher;

/**
 * 
 * 日历左右滑动的视图(未实现) 参考calendar的DayView的源代码
 * 在View中的ondraw函数画viewswitcer的curView和nextView 同时用gesturelistener来监听滑动的offset
 * 记录：不要用viewgroup 那样需要不停的添加移除childview 而且也不会调用ondraw而是调用ondispatchdraw
 */

public class WeekView extends View implements OnGestureListener {

	private GestureDetector mDetector;

	public WeekView(Context context, ViewSwitcher switcher) {
		super(context);
		mSwitcher = switcher;
		mDetector = new GestureDetector(context, this);
		setBackgroundColor(Color.GREEN);
		mTouchMode = TOUCH_MODE_INITIAL_SCROLL;
	}

	private ViewSwitcher mSwitcher;
	private final int TOUCH_MODE_INITIAL_STATE = 1;
	private final int TOUCH_MODE_INITIAL_SCROLL = 2;
	private int mTouchMode = TOUCH_MODE_INITIAL_STATE;
	

	@Override
	protected void onDraw(Canvas canvas) {
		System.out.println("========onDraw=====");
		if (mTouchMode == TOUCH_MODE_INITIAL_SCROLL) {
			canvas.save();
			canvas.translate(300, 300);

			paint.setColor(Color.RED);
			paint.setTextSize(40);
			canvas.drawText("=2222=", 150, 50, paint );
			canvas.restore();
			WeekView nextView = (WeekView) mSwitcher.getNextView();
			nextView.layout(100, 100, 300, 300);
			nextView.mTouchMode = TOUCH_MODE_INITIAL_STATE;
			nextView.setBackgroundColor(Color.BLUE);
			nextView.draw(canvas);
		} else {
			canvas.save();
			canvas.translate(-200, 0);
			
			paint.setColor(Color.RED);
			paint.setTextSize(40);
			canvas.drawText("=11111=", 200, 100, paint );
			canvas.restore();
		}
	}
	Paint paint = new Paint();
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		return super.onTouchEvent(event);
	}

	/*
	 * gesture detector function
	 */

	@Override
	public boolean onDown(MotionEvent e) {
		return false;
	}

	@Override
	public void onShowPress(MotionEvent e) {

	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		return false;
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {
		return false;
	}

	@Override
	public void onLongPress(MotionEvent e) {

	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		return false;
	}

}
