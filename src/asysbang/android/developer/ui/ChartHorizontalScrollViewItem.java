package asysbang.android.developer.ui;


import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;
import asysbang.android.developer.R;

public class ChartHorizontalScrollViewItem extends LinearLayout {
	
	TextView mark;

	public ChartHorizontalScrollViewItem(Context context, String markStr) {
		super(context);
		LayoutInflater factory = LayoutInflater.from(context);
		factory.inflate(R.layout.chart_horizontal_scroll_view_item, this);
		mark = (TextView) findViewById(R.id.item_mark);
		mark.setText(markStr);
	}

}
