package asysbang.android.developer.ui;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.Scroller;

/**
 * 
 * 可以滑动的柱状图，仿小米运动的效果
 *
 */

public class ChartScrollView extends View {

	private Paint p = new Paint();

	// 记录每个柱状图的宽度，日/周/月时不同
	private int deta;

	// 记录柱状图最大高度
	private int chartHeight;

	// 记录下面标注的文字的高度
	private int markHeight;

	// 下面标注文字的padding值，只计算上下padding
	private int markPadding = 10;

	private Calendar selectedCalendar;

	// 记录触摸(点击和滑动)事件最后一次的x位置
	private float mLastMotionX;
	// 滑动组件
	private Scroller mScroller;
	// 水平滑动的距离
	private int mMovedX;

	// 记录画布宽度
	private int canvasWidth;

	private int mAnimationDuration = 350;

	private boolean mIsBeingDragged = false;

	private VelocityTracker mVelocityTracker;
	// 记录当前的柱状图的索引
	private int curIndex;
	
	//柱状图之间的间距
	private int chartSpace = 2;
	
	// 触摸的几种状态
	private final static int TOUCH_STATE_REST = 0;
	private final static int TOUCH_STATE_SCROLLING = 1;
	private final static int TOUCH_STATE_FLINGING = 2;
	private int mTouchState = TOUCH_STATE_REST;

	private List<Integer> data = new ArrayList<Integer>();
	private List<String> dataMark = new ArrayList<String>();

	private int mMinimumVelocity;

	private int mMaximumVelocity;

	private long beginTime, endTime;

	public ChartScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
		selectedCalendar = Calendar.getInstance();
		final ViewConfiguration configuration = ViewConfiguration.get(context);
		mMinimumVelocity = configuration.getScaledMinimumFlingVelocity();
		mMaximumVelocity = configuration.getScaledMaximumFlingVelocity();
		mScroller = new Scroller(context);
		p.setColor(Color.WHITE);
		p.setTextSize(30);
		p.setAlpha(60);
		mockupData();
		Random rand = new Random(System.currentTimeMillis());
		for (long time = beginTime; time < endTime;) {
			data.add(rand.nextInt(100));
			Calendar c = Calendar.getInstance();
			c.setTimeInMillis(time);
			int month = c.get(Calendar.MONTH) + 1;
			dataMark.add(month + "-" + c.get(Calendar.DAY_OF_MONTH));
			time += 24 * 3600 * 1000;
		}
		curIndex = 6;
	}

	private void mockupData() {
		// 月从0 开始计算
		Date d = new Date(2015, 3, 23);
		beginTime = d.getTime();
		Date d1 = new Date(2015, 5, 11);
		endTime = d1.getTime();
	}

	@Override
	public void computeScroll() {
		if (mScroller.computeScrollOffset()) {
			scrollTo(mScroller.getCurrX(), 0);
			postInvalidate();
		}
	}
	
	private int cutType = 0;

	@Override
	protected void onDraw(Canvas canvas) {
		canvasWidth = canvas.getWidth();
		chartHeight = canvas.getHeight() - 100;
		deta = canvasWidth / 7;
		canvas.save();
		canvas.translate(-getCenter(deta, curIndex) - mMovedX, 0);
		int temLeft = curIndex * (deta +chartSpace);
		int temCurIndex = (temLeft +mMovedX +deta /2 )/(deta+chartSpace);
		int left = 0;
		for (int i = 0; i < data.size(); i++) {
			int top = chartHeight - chartHeight * data.get(i) / 100;
			Rect r = new Rect(left, top, left + deta, chartHeight);
			if (i == temCurIndex) {
				Paint p1 = new Paint();
				p1.setColor(Color.WHITE);
				p1.setTextSize(30);
				p1.setAlpha(180);
				canvas.drawRect(r, p1);
			} else {
				canvas.drawRect(r, p);
			}
			String mark = dataMark.get(i);
			Rect bounds = new Rect();
			p.getTextBounds(mark, 0, mark.length(), bounds);
			int textLeftPadding = (deta - bounds.width()) / 2;
			canvas.drawText(mark, left + textLeftPadding, chartHeight + 50, p);
			left = left + deta + chartSpace;
		}
		canvas.restore();
		Paint p1 = new Paint();
		p1.setColor(Color.WHITE);
		p1.setTextSize(30);
		
		int center = canvasWidth / 2 + mScroller.getCurrX();
        Path path = new Path();  
        path.moveTo(center,chartHeight-10);// 此点为多边形的起点  
        path.lineTo(center-10, chartHeight);  
        path.lineTo(center+10, chartHeight);  
        path.close(); // 使这些点构成封闭的多边形  
        canvas.drawPath(path, p1); 
	}

	// 计算第index个的中间位置
	private int getCenter(int deta, int index) {
		return index * (deta + chartSpace) + deta / 2 - canvasWidth / 2;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		initVelocityTrackerIfNotExists();
		mVelocityTracker.addMovement(event);
		if (!mScroller.isFinished()) {
			return true;
		}
		int action = event.getAction();
		switch (action) {
		case MotionEvent.ACTION_DOWN:
			if (!mScroller.isFinished()) {
				mScroller.abortAnimation();
			}
			mLastMotionX = event.getX();
			mTouchState = mScroller.isFinished() ? TOUCH_STATE_REST : TOUCH_STATE_SCROLLING;
			break;
		case MotionEvent.ACTION_MOVE:
			mTouchState = TOUCH_STATE_SCROLLING;
			final float x = event.getX();
			final int xDiff = (int) (mLastMotionX - x);
			mMovedX += xDiff;
			mLastMotionX = x;
			invalidate();
			break;
		case MotionEvent.ACTION_UP:
			final VelocityTracker velocityTracker = mVelocityTracker;
			int left = curIndex * (deta +chartSpace);
			curIndex = (left +mMovedX +deta /2 )/(deta+chartSpace);
			mMovedX= 0;
			invalidate();
			recycleVelocityTracker();
			break;

		default:
			break;
		}
		return super.onTouchEvent(event);
	}

	private void initOrResetVelocityTracker() {
		if (mVelocityTracker == null) {
			mVelocityTracker = VelocityTracker.obtain();
		} else {
			mVelocityTracker.clear();
		}
	}

	private void initVelocityTrackerIfNotExists() {
		if (mVelocityTracker == null) {
			mVelocityTracker = VelocityTracker.obtain();
		}
	}

	private void recycleVelocityTracker() {
		if (mVelocityTracker != null) {
			mVelocityTracker.recycle();
			mVelocityTracker = null;
		}
	}

}
