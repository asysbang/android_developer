package asysbang.android.developer.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteCallbackList;
import android.os.RemoteException;

public class MainService extends Service {

	private IMainService.Stub mBinder = new IMainService.Stub() {

		@Override
		public void getDataFromServer() throws RemoteException {
			//模拟访问网络，等待数据。。。
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			onDataLoaded(0,"xxxx");
		}

		@Override
		public void registerCallback(IMainServiceCallback cb) throws RemoteException {
			if (cb != null) {
				mCallback.register(cb);
			}
		}

		@Override
		public void unregisterCallback(IMainServiceCallback cb) throws RemoteException {
			if (cb != null) {
				mCallback.unregister(cb);
			}
		}
	};

	final RemoteCallbackList<IMainServiceCallback> mCallback = new RemoteCallbackList<IMainServiceCallback>();

	private void onDataLoaded(int flag ,String msg) {
		try {
			final int N = mCallback.beginBroadcast();
			for (int i = 0; i < N; i++) {
				mCallback.getBroadcastItem(i).onDataLoaded(flag, msg);
				mCallback.finishBroadcast();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}

}
