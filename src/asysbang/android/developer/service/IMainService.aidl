package asysbang.android.developer.service;

import asysbang.android.developer.service.IMainServiceCallback;
interface IMainService {

	void getDataFromServer();
	
	    void registerCallback(IMainServiceCallback cb);
	
	void unregisterCallback(IMainServiceCallback cb);
}