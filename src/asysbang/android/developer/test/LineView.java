package asysbang.android.developer.test;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathEffect;
import android.util.AttributeSet;
import android.view.View;

public class LineView extends View {

	public LineView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	PathEffect effects = new DashPathEffect(new float[]{5,5,5,5},1);  
	Paint paint = new Paint();
	@Override
	protected void onDraw(Canvas canvas) {
		paint.setAntiAlias(true);
		paint.setStyle(Paint.Style.STROKE);  
		paint.setColor(Color.BLUE);
		paint.setPathEffect(effects);
		canvas.drawCircle(202, 202, 100, paint);
//		canvas.drawLine(0, 0, 400, 400, paint);
		
		Path path = new Path();
		path.moveTo(10, 10);
		path.lineTo(400, 400);
		canvas.drawPath(path , paint);

	}

}
